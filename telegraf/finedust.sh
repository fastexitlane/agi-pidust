#! /bin/bash

stty -F /dev/ttyUSB0 9600 raw

INPUT=$(od --endian=big -x -N10 < /dev/ttyUSB0|head -n 1|cut -f2-10 -d" ");

FIRST4BYTES=$(echo $INPUT|cut -b1-4);

PPM25LOW=$(echo $INPUT|cut -f2 -d " "|cut -b1-2);
PPM25LOWDEC=$( echo $((0x$PPM25LOW)) );
PPM25HIGH=$(echo $INPUT|cut -f2 -d " "|cut -b3-4);
PPM25HIGHDEC=$( echo $((0x$PPM25HIGH)) );

PPM10LOW=$(echo $INPUT|cut -f3 -d " "|cut -b1-2);
PPM10LOWDEC=$( echo $((0x$PPM10LOW)) );
PPM10HIGH=$(echo $INPUT|cut -f3 -d " "|cut -b3-4);
PPM10HIGHDEC=$( echo $((0x$PPM10HIGH)));

PPM25LOWDEC=`bc <<< "scale=2; $PPM25LOWDEC/10"`;
PPM25HIGHDEC=`bc <<< "scale=2; $PPM25HIGHDEC/10"`;
PPM10LOWDEC=`bc <<< "scale=2; $PPM10LOWDEC/10"`;
PPM10HIGHDEC=`bc <<< "scale=2; $PPM10HIGHDEC/10"`;

echo "finedust PPM10LOW=$PPM10LOWDEC,PPM10HIGH=$PPM10HIGHDEC,PPM25LOW=$PPM25LOWDEC,PPM25HIGH=$PPM25HIGHDEC"

exit 0
