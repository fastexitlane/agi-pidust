#! /bin/bash

lat=`gpspipe -w | grep -m 1 "lat" | grep -oP ".lat..([0-9]*.[0-9]*)" | grep -oP "[0-9]+.[0-9]+"`
lon=`gpspipe -w | grep -m 1 "lon" | grep -oP ".lon..([0-9]*.[0-9]*)" | grep -oP "[0-9]+.[0-9]+"`

echo "gps lat=$lat,lon=$lon"

exit 0
