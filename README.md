# PiDust - Feinstaubmessung mit dem Raspberry Pi und dem Influx-Stack
<!-- TOC -->

- [WLAN-Netz](#wlan-netz)
- [SSH-Zugang](#ssh-zugang)
- [Seiten](#seiten)
- [Anwendungen / Ports](#anwendungen--ports)
- [GPIO Map](#gpio-map)
- [Source Files](#source-files)

<!-- /TOC -->

***

## WLAN-Netz
Netzname: `finedust`

WPA2-Key: `finedust`


## SSH-Zugang
IP-Adresse des Pi im WLAN: `192.168.1.1` (auch DNS-Server / Router)

SSH-User: `pi`

SSH-Passwort: `raspberry`


## Seiten
Grafana: `grafana.pi`

Chronograf: `chronograf.pi`


## Anwendungen / Ports
| Port | Anwendung          |
|------|--------------------|
| 8888 | Chronograf         |
| 8086 | REST-API InfluxDB  |
| 3000 | Grafana            |
| 9092 | REST-API Kapacitor |


## GPIO Map
| Pin | Belegung           | wPi # |
|-----|--------------------|-------|
| 1   | GPS 3.3 V          |       |
| 8   | GPS TxD (to GPS)   |       |
| 9   | GPS GND            |       |
| 10  | GPS RxD (from GPS) |       |
| 20  | LED Gelb (GND)     |       |
| 22  | LED Gelb (-)       | 6     |
| 30  | LED Grün (GND)     |       |
| 32  | LED Grün (-)       | 26    |
| 34  | LED Rot (GND)      |       |
| 36  | LED Rot (-)        | 27    |


## Source Files
`apache2/`: Konfigurationsdateien für Webserver (Reverse Proxy für Grafana und Chronograf)

`kapacitor/`: TICKscript für Schwellenauswertung, Steuerungsskript für die LEDs (GPIO)

`telegraf/`: Bash-Skripte für Datenabholung von UNIX-Devices (Feinstaubsensor, GPS-Tracker)
