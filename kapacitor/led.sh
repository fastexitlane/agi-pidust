#! /bin/bash

RED=27
YELLOW=6
GREEN=26

gpio write $RED 0
gpio write $YELLOW 0
gpio write $GREEN 0

case "$1" in
        RED)
                gpio write $RED 1
                ;;
        GREEN)
                gpio write $GREEN 1
                ;;
        YELLOW)
                gpio write $YELLOW 1
                ;;
esac

